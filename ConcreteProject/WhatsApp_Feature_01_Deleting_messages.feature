# language: en

Feature: Deleting messages
	The user should be able to delete any message from the chats

Scenario: Delete a message from a single chat
	Given I am in a single chat opened
	When I press and hold a message
	Then a trash can icon will be displayed at top bar
	When clicking on trash can icon
	Then a pop-up will be displayed asking to confirm the deletion
	When clicking to confirm the deletion
	Then the desired message will disappear from the chat 

Scenario: Delete a sent message from a group chat before someone sees
	Given I am in a group chat opened
	And I just sent a message 
	And I regret having sent
	When pressing and hold the sent message
	Then a trash can icon will be displayed at top bar
	When clicking on trash can icon
	Then a pop-up will be displayed asking to confirm to delete for me, cancel or delete for everyone
	When clicking to delete for everyone
	Then people who have not seen the message yet will not see anymore
