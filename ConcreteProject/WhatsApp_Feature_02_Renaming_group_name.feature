# language: en

Feature: Renaming group name
	The user should be able to rename any group name he is a member

Scenario: Renaming a group name from group screen
	Given I am in a group chat opened
	When I press the group name at top bar
	Then the group info will be displayed containing a pencil
	When clicking on the pencil
	Then a screen will be displayed allowing to rename the group name
	When typing a new name
	And clicking on OK button to confirm the edition
	Then the new group name will be displayed on the group info screen 

Scenario: Renaming a group name with non-ASCII characters from chats tab screen
	Given I am in the chat tab at WhatsApp main screen
	When I press in the group picture 
	Then the group screen preview will be displayed containing the info button
	When clicking on the info button
	Then a screen will be displayed allowing to rename the group name
	When typing a new name containing non-ASCII characters (e.g.: chinese, japanese, etc)
	And clicking on OK button to confirm the edition
	Then the new group name containing non-ASCII characters should be properly displayed on the group info screen 
	Then the new group name containing non-ASCII characters should be properly displayed in the chat tab at WhatsApp main screen