import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by alexcandido on 06/12/17.
 */
public class CheckoutBasket {

    private String shoppingList;

    public CheckoutBasket() {
        shoppingList = "";
    }

    void addToShoppingList(String itemName) {
        this.shoppingList += itemName;
    }

    int calculateShoppingListPrice() {

        int totalValue = 0;
        int amountOfProductA = 0;
        int amountOfProductB = 0;

        for (int i = 0; i < shoppingList.length(); i++) {
            switch (shoppingList.charAt(i)) {

                case ProductsAndRules.PROD_A:
                    amountOfProductA += 1;

                    if (amountOfProductA == ProductsAndRules.AMOUNT_FOR_DISCOUNT_PROD_A) {
                        amountOfProductA = 0;
                        totalValue += (ProductsAndRules.PRICE_PROD_A - ProductsAndRules.DISCOUNT_PROD_A);
                    } else {
                        totalValue += ProductsAndRules.PRICE_PROD_A;
                    }
                    break;

                case ProductsAndRules.PROD_B:
                    amountOfProductB += 1;

                    if (amountOfProductB == ProductsAndRules.AMOUNT_FOR_DISCOUNT_PROD_B) {
                        amountOfProductB = 0;
                        totalValue += (ProductsAndRules.PRICE_PROD_B - ProductsAndRules.DISCOUNT_PROD_B);
                    } else {
                        totalValue += ProductsAndRules.PRICE_PROD_B;
                    }
                    break;

                case ProductsAndRules.PROD_C:
                    totalValue += ProductsAndRules.PRICE_PROD_C;
                    break;

                case ProductsAndRules.PROD_D:
                    totalValue += ProductsAndRules.PRICE_PROD_D;
                    break;

                default:
                    break;
            }
        }
        return totalValue;
    }
}
