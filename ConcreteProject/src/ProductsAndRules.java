/**
 * Created by alexcandido on 06/12/17.
 */
public class ProductsAndRules {

    // PRODUCTS AND DISCOUNT RULES
    public static final char PROD_A = 'A';
    public static final char PROD_B = 'B';
    public static final char PROD_C = 'C';
    public static final char PROD_D = 'D';

    public static final int PRICE_PROD_A = 50;
    public static final int PRICE_PROD_B = 30;
    public static final int PRICE_PROD_C = 20;
    public static final int PRICE_PROD_D = 15;

    public static final int AMOUNT_FOR_DISCOUNT_PROD_A = 3;
    public static final int AMOUNT_FOR_DISCOUNT_PROD_B = 2;

    public static final int DISCOUNT_PROD_A = 20;
    public static final int DISCOUNT_PROD_B = 15;

}
