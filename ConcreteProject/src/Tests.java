import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by alexcandido on 06/12/17.
 */
public class Tests {

    private CheckoutBasket cb;

    public int calculateShoppingListPrice(String product){
        this.cb = new CheckoutBasket();

        cb.addToShoppingList(product);
        return cb.calculateShoppingListPrice();
    }

    @Test
    public void testPartialPrice() {
        this.cb = new CheckoutBasket();

        assertEquals(0, cb.calculateShoppingListPrice());

        cb.addToShoppingList("A"); assertEquals(50, cb.calculateShoppingListPrice());
        cb.addToShoppingList("B"); assertEquals(80, cb.calculateShoppingListPrice());
        cb.addToShoppingList("A"); assertEquals(130, cb.calculateShoppingListPrice());
        cb.addToShoppingList("A"); assertEquals(160, cb.calculateShoppingListPrice());
        cb.addToShoppingList("B"); assertEquals(175, cb.calculateShoppingListPrice());

    }

    @Test
    public void testTotalPrice(){

        assertEquals(  0, calculateShoppingListPrice(""));
        assertEquals( 50, calculateShoppingListPrice("A"));
        assertEquals( 80, calculateShoppingListPrice("AB"));
        assertEquals(115, calculateShoppingListPrice("CDBA"));

        assertEquals(100, calculateShoppingListPrice("AA"));
        assertEquals(130, calculateShoppingListPrice("AAA"));
        assertEquals(180, calculateShoppingListPrice("AAAA"));
        assertEquals(230, calculateShoppingListPrice("AAAAA"));
        assertEquals(260, calculateShoppingListPrice("AAAAAA"));

        assertEquals(160, calculateShoppingListPrice("AAAB"));
        assertEquals(175, calculateShoppingListPrice("AAABB"));
        assertEquals(190, calculateShoppingListPrice("AAABBD"));
        assertEquals(190, calculateShoppingListPrice("DABABA"));

    }
}
